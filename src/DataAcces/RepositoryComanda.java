package DataAcces;


import Model.Comanda;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class RepositoryComanda {

        public RepositoryComanda(Connectionn utils) {
            this.utils = utils;
        }

        private Connectionn utils;

        public RepositoryComanda(Properties propsss) {
            utils = new Connectionn(propsss);
        }

    public List<Comanda> findAll(){
        Connection con = utils.getConnection();
        List<Comanda> comenzi = new ArrayList<>();
        try(PreparedStatement preStmt = con.prepareStatement("Select c.*,p.tip_produs, ctr.cantitate from comanda c inner join control_comanda ctr on ctr.id_comanda = c.numar_comanda inner join produs p on ctr.tip_produs = p.tip_produs")){
            try(ResultSet result = preStmt.executeQuery()){
                while(result.next()){
                    Integer numar_comanda = result.getInt("numar_comanda");
                    Integer id_client = result.getInt("id_client");
                    String tip_produs =result.getString("tip_produs");
                    Integer cantitate = result.getInt("cantitate");


                    Comanda c = new Comanda(numar_comanda,id_client,tip_produs,cantitate);
                    comenzi.add(c);
                }
            }
        }catch (SQLException ex){
            System.out.println("Eroare DB " + ex);
        }
        return comenzi;
    }

    public void adauga(Comanda entity){

        Connection con = utils.getConnection();
        try(PreparedStatement preStmt = con.prepareStatement("insert into comanda(numar_comanda,id_client) values (?,?)")){

            preStmt.setInt(1, entity.getNumar_comanda());
            preStmt.setInt(2, entity.getId_client());

            int result = preStmt.executeUpdate();
        }catch(SQLException ex){
            System.out.println("Eroare DB " + ex);
        }
        try(PreparedStatement preStmt = con.prepareStatement("insert into control_comanda(id_comanda,tip_produs,cantitate) values (?,?,?)")){

            preStmt.setInt(1, entity.getNumar_comanda());
            preStmt.setString(2, entity.getTip_produs());
            preStmt.setInt(3, entity.getCantitate());

            int result = preStmt.executeUpdate();
        }catch(SQLException ex){
            System.out.println("Eroare DB " + ex);
        }
    }




    }


