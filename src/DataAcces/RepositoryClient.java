package DataAcces;


import Model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class RepositoryClient {
    private Connectionn utils;
    public RepositoryClient(Properties props){
        utils=new Connectionn(props);
    }

    public List<Client> findAll(){
        Connection con = utils.getConnection();
        List<Client> clienti = new ArrayList<>();
        try(PreparedStatement preStmt = con.prepareStatement("select * from client")){
            try(ResultSet result = preStmt.executeQuery()){
                while(result.next()){
                    Integer id = result.getInt("id");
                    String nume = result.getString("nume");


                    Client c = new Client(id, nume);
                   clienti.add(c);
                }
            }
        }catch (SQLException ex){
            System.out.println("Eroare DB " + ex);
        }
        return clienti;
    }


    public int size(){
        Connection con = utils.getConnection();
        try(PreparedStatement preStmt = con.prepareStatement("select count(*) as [SIZE] from client")){
            try(ResultSet result = preStmt.executeQuery()){
                if (result.next()){
                    return result.getInt("SIZE");
                }
            }
        }catch(SQLException ex){
            System.out.println("Eroare DB " + ex);
        }
        return 0;
    }


    public void adauga(Client entity){
        Connection con = utils.getConnection();
        try(PreparedStatement preStmt = con.prepareStatement("insert into client(nume) values (?)")){
            //preStmt.setInt(1, entity.getID());
            preStmt.setString(1, entity.getNume());

            int result = preStmt.executeUpdate();
        }catch(SQLException ex){
            System.out.println("Eroare DB " + ex);
        }
    }


    public void delete(Integer id) {
        Connection con=utils.getConnection();
        try(PreparedStatement preStmt=con.prepareStatement("delete from client where id=?")){
            preStmt.setInt(1,id);
            int result=preStmt.executeUpdate();
        }catch (SQLException ex){
            System.out.println("Eroare DB " + ex);
        }
    }


    public void update(Integer integer, Client entity) {
        Connection con = utils.getConnection();
        try(PreparedStatement preStmt = con.prepareStatement("update client set nume = ?  where id=?")){

            preStmt.setString(1, entity.getNume());
            preStmt.setInt(2, integer);

            int result = preStmt.executeUpdate();
        }catch(SQLException ex){
            System.out.println("Eroare DB " + ex);
        }
    }


    public Client findOne(Integer id){
        Connection con = utils.getConnection();

        try(PreparedStatement preStmt = con.prepareStatement("select * from client where id = ?")){
            preStmt.setInt(1, id);
            try(ResultSet result = preStmt.executeQuery()){
                if (result.next()){
                    Integer idd = result.getInt("id");
                    String nume = result.getString("nume");


                   Client c = new Client(id,nume);
                    return c;
                }
            }
        }catch (SQLException ex){
            System.out.println("Eroare DB " + ex);
        }
        return null;
    }







}
