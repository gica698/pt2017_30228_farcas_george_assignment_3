package DataAcces;


import Model.Produs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class RepositoryProdus {
    public RepositoryProdus(Connectionn utils) {
        this.utils = utils;
    }

    private Connectionn utils;

    public RepositoryProdus(Properties propss) {
        utils = new Connectionn(propss);
    }

    public List<Produs> findAll() {
        Connection con = utils.getConnection();
        List<Produs> produse = new ArrayList<>();
        try (PreparedStatement preStmt = con.prepareStatement("select * from produs")) {
            try (ResultSet result = preStmt.executeQuery()) {
                while (result.next()) {
                    Integer id=result.getInt("id");
                    String tip_produs = result.getString("tip_produs");
                    Integer cantitate = result.getInt("cantitate");


                    Produs p = new Produs(tip_produs, cantitate,id);
                    produse.add(p);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Eroare DB " + ex);
        }
        return produse;
    }


    public void adauga(Produs entity){
        Connection con = utils.getConnection();
        try(PreparedStatement preStmt = con.prepareStatement("insert into produs(tip_produs,cantitate) values (?,?)")){
            //preStmt.setInt(1, entity.getID());
            //preStmt.setInt(1, entity.getId());
            preStmt.setString(1, entity.getTip_produs());
            preStmt.setInt(2, entity.getCantitate());

            int result = preStmt.executeUpdate();
        }catch(SQLException ex){
            System.out.println("Eroare DB " + ex);
        }
    }

    public void delete(Integer id) {
        Connection con=utils.getConnection();
        try(PreparedStatement preStmt=con.prepareStatement("delete from produs where id=?")){
            preStmt.setInt(1,id);
            int result=preStmt.executeUpdate();
        }catch (SQLException ex){
            System.out.println("Eroare DB " + ex);
        }
    }

    public void update(Integer integer, Produs entity) {
        Connection con = utils.getConnection();
        try(PreparedStatement preStmt = con.prepareStatement("update produs set tip_produs=?,cantitate = ?  where id=?")){

            preStmt.setString(1, entity.getTip_produs());
            preStmt.setInt(2, entity.getCantitate());
            preStmt.setInt(3, integer);

            int result = preStmt.executeUpdate();
        }catch(SQLException ex){
            System.out.println("Eroare DB " + ex);
        }
    }
}