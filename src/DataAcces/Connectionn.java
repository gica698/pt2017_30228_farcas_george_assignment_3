package DataAcces;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Connectionn {
    private Properties props;

    public Connectionn(Properties props) {
        this.props = props;
    }

    private Connection instance = null;

    private Connection getNewConnection() {
        String driver = props.getProperty("DRIVER");
        String url = props.getProperty("BURL");
        String user = props.getProperty("USER");
        String pass = props.getProperty("PASS");
        Connection con = null;
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url,user,pass);

        } catch (ClassNotFoundException e) {
            System.out.println("Error loading driver " + e);
        } catch (SQLException e) {
            System.out.println("Error getting connection " + e);
        }
        return con;
    }

    public Connection getConnection() {
        try {
            if (instance == null || instance.isClosed())
                instance = getNewConnection();

        } catch (SQLException e) {
            System.out.println("Eroare DB " + e);
        }
        return instance;
    }
}



