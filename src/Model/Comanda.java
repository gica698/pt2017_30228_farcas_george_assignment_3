package Model;


public class Comanda {
    Integer numar_comanda;
    Integer id_client;
    String tip_produs;
    Integer cantitate;

    public Integer getNumar_comanda() {
        return numar_comanda;
    }

    public void setNumar_comanda(Integer numar_comanda) {
        this.numar_comanda = numar_comanda;
    }

    public Integer getId_client() {
        return id_client;
    }

    public void setId_client(Integer id_client) {
        this.id_client = id_client;
    }

    public String getTip_produs() {
        return tip_produs;
    }

    public void setTip_produs(String tip_produs) {
        this.tip_produs = tip_produs;
    }

    public Integer getCantitate() {
        return cantitate;
    }

    public void setCantitate(Integer cantitate) {
        this.cantitate = cantitate;
    }

    public Comanda(Integer numar_comanda, Integer id_client, String tip_produs, Integer cantitate) {

        this.numar_comanda = numar_comanda;
        this.id_client = id_client;
        this.tip_produs = tip_produs;
        this.cantitate = cantitate;
    }

    public Comanda(Integer numar_comanda, Integer id_client) {

        this.numar_comanda = numar_comanda;

        this.id_client = id_client;
    }

    public Comanda(Integer id_client) {
        this.id_client = id_client;
    }
}
