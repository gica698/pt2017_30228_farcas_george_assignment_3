package Model;


public class Produs {

    String tip_produs;
    Integer cantitate;
    Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTip_produs() {
        return tip_produs;
    }

    public void setTip_produs(String tip_produs) {
        this.tip_produs = tip_produs;
    }

    public Integer getCantitate() {
        return cantitate;
    }

    public void setCantitate(Integer cantitate) {
        this.cantitate = cantitate;
    }

    public Produs( String tip_produs, Integer cantitate,Integer id) {


        this.tip_produs = tip_produs;
        this.cantitate = cantitate;
        this.id = id;
    }

    public Produs(String tip_produs, Integer cantitate) {
        this.tip_produs = tip_produs;
        this.cantitate = cantitate;
    }
}
