package Model;


public class Client {
    Integer id;
    String nume;

    public Client(String nume) {
        this.nume = nume;
    }

    public Client(Integer id) {
        this.id = id;
    }



    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Client(Integer id, String nume) {

        this.id = id;
        this.nume = nume;
    }
}
