package BusinessLogic;

import DataAcces.RepositoryProdus;
import Model.Produs;

import java.util.List;

public class ControlProdus {
    public ControlProdus(RepositoryProdus repo) {
        this.repo = repo;
    }

    RepositoryProdus repo ;

    public void adaugaProdus(Produs produs){
        repo.adauga(produs);
    }

    public void updateProdus(Integer integer,Produs produs){
        repo.update(integer,produs);
    }

    public void deleteProdus(Integer id){
        repo.delete(id);
    }

    public List<Produs> showProdus(){
        return repo.findAll();

    }
}
