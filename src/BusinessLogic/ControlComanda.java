package BusinessLogic;

import DataAcces.RepositoryComanda;
import Model.Comanda;

import java.util.List;

public class ControlComanda {
    public ControlComanda(RepositoryComanda repo) {
        this.repo = repo;
    }

    RepositoryComanda repo ;

    public void adaugaComanda(Comanda comanda){
        repo.adauga(comanda);
    }

    public List<Comanda> showComanda(){
        return repo.findAll();

    }
}
