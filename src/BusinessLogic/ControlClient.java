package BusinessLogic;

import DataAcces.RepositoryClient;
import Model.Client;

import java.util.List;

public class ControlClient {
    RepositoryClient repo ;

    public ControlClient(RepositoryClient repo) {
        this.repo = repo;
    }

    public void adaugaClient(Client client){
        repo.adauga(client);
    }

    public void deleteClient(Integer id){
        repo.delete(id);
    }

    public void updateClient(Integer integer,Client client){
        repo.update(integer,client);
    }

    public List<Client> showClient(){
        return repo.findAll();

    }
}
