import BusinessLogic.ControlClient;
import BusinessLogic.ControlComanda;
import BusinessLogic.ControlProdus;
import DataAcces.RepositoryClient;
import DataAcces.RepositoryComanda;
import DataAcces.RepositoryProdus;
import UI.ControllerClient;
import UI.ControllerComanda;
import UI.ControllerProdus;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Properties props = new Properties();


        try {
            props.load(new FileReader("bd.config"));
            props.list(System.out);


        } catch (IOException e) {
            e.printStackTrace();

        }
        Stage stage1 = new Stage();
        RepositoryClient repo1 = new RepositoryClient(props);
        ControlClient ctrl1 = new ControlClient(repo1);
        FXMLLoader loader1 = new FXMLLoader();
        loader1.setLocation(Main.class.getResource("UI/Client.fxml"));
        AnchorPane anchorPane1 = loader1.load();
        ControllerClient ctrlclient = loader1.getController();
        ctrlclient.setControl(ctrl1);
        Scene scene1 = new Scene(anchorPane1);
        stage1.setScene(scene1);
        stage1.setTitle("Client");
        stage1.show();





        try

        {
            props.load(new FileReader("bd.config"));
            props.list(System.out);


        } catch (
                IOException e)

        {
            e.printStackTrace();

        }
        Stage stage2 = new Stage();
        RepositoryProdus reepo = new RepositoryProdus(props);
        ControlProdus ctrll = new ControlProdus(reepo);
        FXMLLoader loaderr = new FXMLLoader();
        loaderr.setLocation(Main.class.

                getResource("UI/Produs.fxml"));
        AnchorPane anchorPanee = loaderr.load();
        ControllerProdus ctrlProdus = loaderr.getController();
        ctrlProdus.setControl(ctrll);
        Scene scenee = new Scene(anchorPanee);
        stage2.setScene(scenee);
        stage2.setTitle("produse");
        stage2.show();










        try {
            props.load(new FileReader("bd.config"));
            props.list(System.out);


        } catch (IOException e) {
            e.printStackTrace();

        }

        RepositoryComanda repo = new RepositoryComanda(props);
        ControlComanda ctrl = new ControlComanda(repo);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("UI/ControlComanda.fxml"));
        AnchorPane anchorPane = loader.load();
        ControllerComanda ctrlcomanda = loader.getController();
        ctrlcomanda.setControl(ctrl);
        Scene scene = new Scene(anchorPane);
        stage.setScene(scene);
        stage.setTitle("Comanda");
        stage.show();






















    }
    public static void main(String[] a){
        launch(a);
    }
}

/*public class Main {
    public static void main(String arg[]){
        Properties props=new Properties();
        Properties propss=new Properties();
        //System.out.println(new File(".").getAbsoluteFile());
        try{
            props.load(new FileReader("bd.config"));
            props.list(System.out);
            propss.load(new FileReader("bd.config"));
            propss.list(System.out);
        }catch(IOException e){
            e.printStackTrace();

        }

        RepositoryClient repo=new RepositoryClient(props);
        repo.adauga(new Client("Iuli"));
        repo.update(1,new Client("Cotoi"));
        repo.delete(2);
        System.out.println(repo.findAll().size());
        for(Client c : repo.findAll()){
            System.out.println(c.getNume());
        }

        RepositoryProdus rep=new RepositoryProdus(propss);
        rep.adauga(new Produs("PCC",5));
        rep.delete(6);
        rep.update(4,new Produs("Telefonnn",13));

        for(Produs p:rep.findAll()){
            System.out.println(p.getTip_produs());
            System.out.println(p.getCantitate());
        }


    }
}*/
