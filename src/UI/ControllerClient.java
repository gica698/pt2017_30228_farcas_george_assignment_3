package UI;


import BusinessLogic.ControlClient;
import Model.Client;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.Observable;
import java.util.ResourceBundle;


public class ControllerClient implements Initializable {
    @FXML
    private TableView<Client> TabelClienti;
    @FXML
    private TableColumn IdCol;
    @FXML
    private TableColumn NumeCol;
    @FXML
    private TextField NumeText;
    @FXML
    private TextField Idtext;
    @FXML
    private Button AdaugaButon;
    @FXML
    private Button StergeButon;
    @FXML
    private Button UpdateButon;

    private ControlClient control;
    private ObservableList<Client> modelclient;

    public ControllerClient() {

    }

    public void setControl(ControlClient c){
        this.control = c;
        init();
    }

    public void init() {
        modelclient = FXCollections.observableArrayList(control.showClient());
        TabelClienti.setItems(modelclient);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        IdCol.setCellValueFactory(new PropertyValueFactory<Client, Integer>("Id"));
        NumeCol.setCellValueFactory(new PropertyValueFactory<Client,String>("Nume"));
    }

    public void AdaugaButonHandler(){
        String nume=NumeText.getText();
        control.adaugaClient(new Client(nume));
        init();
    }
    public void StergeButonHandler(){
        Integer id=Integer.parseInt(Idtext.getText());
        control.deleteClient(id);
        init();
    }
    public void UpdateButonHandler(){
        String nume=NumeText.getText();
        Integer id=Integer.parseInt(Idtext.getText());
        control.updateClient(id,new Client(nume));
        init();
    }


}