package UI;


import BusinessLogic.ControlComanda;
import Model.Comanda;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class ControllerComanda implements Initializable {
    @FXML
    private TableView<Comanda> ComandaTabel;
    @FXML
    private TableColumn IdColC;
    @FXML
    private TableColumn Numar_Comanda;
    @FXML
    private Button adauga;
    @FXML
    private TextField idnrcomanda;
    @FXML
    private TableColumn tip_produscol;
    @FXML
    private TableColumn cantitatecol;
    @FXML
    private TextField id_comanda;
    @FXML
    private TextField tip_produs;
    @FXML
    private TextField cantitate;

    private ControlComanda control;
    private ObservableList<Comanda> modelcomanda;

    public ControllerComanda() {

    }

    public void setControl(ControlComanda c){
        this.control = c;
        init();
    }

    public void init() {
        modelcomanda = FXCollections.observableArrayList(control.showComanda());
        ComandaTabel.setItems(modelcomanda);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        IdColC.setCellValueFactory(new PropertyValueFactory<Comanda, Integer>("id_client"));
        Numar_Comanda.setCellValueFactory(new PropertyValueFactory<Comanda, Integer>("numar_comanda"));
        tip_produscol.setCellValueFactory(new PropertyValueFactory<Comanda, String>("tip_produs"));
        cantitatecol.setCellValueFactory(new PropertyValueFactory<Comanda, Integer>("cantitate"));
    }

    public void AdaugaButonHandler(){

        Integer id_client=Integer.parseInt(idnrcomanda.getText());
        Integer id_comandaa=Integer.parseInt(id_comanda.getText());
        String tip_produss=tip_produs.getText();
        Integer cantitatee=Integer.parseInt(cantitate.getText());
        control.adaugaComanda(new Comanda(id_comandaa,id_client,tip_produss,cantitatee));
        init();
    }

}
