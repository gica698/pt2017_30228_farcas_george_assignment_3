package UI;


import BusinessLogic.ControlProdus;
import Model.Produs;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;


public class ControllerProdus implements Initializable {
    @FXML
    private TableView<Produs> tabelprodus;

    @FXML
    private TableColumn idcol1;
    @FXML
    private TableColumn tip_produscol1;
    @FXML
    private TableColumn cantitatecol1;
    @FXML
    private TextField idtext1;
    @FXML
    private TextField tip_produstext;
    @FXML
    private TextField cantitatetext;
    @FXML
    private Button AdaugaButon1;
    @FXML
    private Button StergeButon1;
    @FXML
    private Button UpdateButon1;

    private ControlProdus control;
    private ObservableList<Produs> modelprodus;

    public ControllerProdus() {

    }

    public void setControl(ControlProdus c){
        this.control = c;
        init();
    }

    public void init() {
        modelprodus = FXCollections.observableArrayList(control.showProdus());
        tabelprodus.setItems(modelprodus);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        idcol1.setCellValueFactory(new PropertyValueFactory<Produs, Integer>("id"));
        tip_produscol1.setCellValueFactory(new PropertyValueFactory<Produs,String>("tip_produs"));
       cantitatecol1.setCellValueFactory(new PropertyValueFactory<Produs,Integer>("cantitate"));
    }

    public void AdaugaButonHandler1(){
        String tip_produs=tip_produstext.getText();
        Integer cantitate=Integer.parseInt(cantitatetext.getText());
        control.adaugaProdus(new Produs(tip_produs,cantitate));
        init();
    }
    public void StergeButonHandler1(){
        Integer id=Integer.parseInt(idtext1.getText());

        control.deleteProdus(id);
        init();
    }
    public void UpdateButonHandler1(){
        String tip_produs=tip_produstext.getText();
        Integer cantitate=Integer.parseInt(cantitatetext.getText());
        Integer id=Integer.parseInt(idtext1.getText());
        control.updateProdus(id,new Produs(tip_produs,cantitate));
        init();
    }


}


